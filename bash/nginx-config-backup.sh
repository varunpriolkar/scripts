#!/bin/bash
cd /home/arhue/git/nginx-configs
git pull
cd /home/arhue/git/scripts/ansible/nginx-config-backup
ansible-playbook /home/arhue/git/scripts/ansible/nginx-config-backup/nginx-configs.yml
cd /home/arhue/git/nginx-configs
git add .
git commit -m "automated ansible config push"
git push
