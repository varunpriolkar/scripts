#!/bin/bash
cd /home/arhue/git/network-configs
git pull
cd /home/arhue/git/scripts/ansible/mikrotik-backup
ansible-playbook /home/arhue/git/scripts/ansible/mikrotik-backup/mikrotik.yml
cd /home/arhue/git/network-configs
git add .
git commit -m "automated ansible config push"
git push
